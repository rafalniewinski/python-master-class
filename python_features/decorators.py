# decorators
from time import time


def timer(func):
    def f(*args, **kwargs):
        before = time()
        rv = func(*args, **kwargs)
        after = time()
        print('elapsed', after - before)
        return rv
    return f


def ntimes(n):
    def inner(f):
        def wrapper(*args, **kwargs):
            for _ in range(n):
                print(_)
                f(*args, **kwargs)
            return f(*args, **kwargs)
        return wrapper
    return inner


@timer
def add(x, y=10):
    return x + y

# add = func(add)


@ntimes(5)
def sub(x, y=10):
    return x - y

# sub = ntimes(sub)


print('add(10)', add(10))
print('add(20, 30)', add(20, 30))
print('sub(20, 5)', sub(20, 5))
