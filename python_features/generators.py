# generators
from time import sleep


def compute():
    rv = []
    for i in range(10):
        sleep(0.5)
        print(i)
        rv.append(i)
    return rv


# generator
class Compute:
    def __iter__(self):
        self.last = 0
        return self

    def __next__(self):
        self.last += 1
        if self.last > 10:
            raise StopIteration()
        sleep(.5)
        return self.last


compute()

# oba metody podają takie same rozwiązania jednak generator nie posiada storages
# nie zapisuje tablicy w pamięci

for val in Compute():
    print(val)


# generatory ładny zapis
def compute():
    for i in range(10):
        sleep(.5)
        yield i


class Api:

    @staticmethod
    def run_this_frist():
        pass

    @staticmethod
    def run_this_secound():
        pass

    @staticmethod
    def run_this_third():
        pass


# wymuszenie kolejności dodatkowy aspekt generatora
def api():
    Api.run_this_frist()
    yield
    Api.run_this_secound()
    yield
    Api.run_this_third()


# this is a list, create all 5000000 x/2 values immediately, uses []
lis = [x/2 for x in range(5000000)]

# this is a generator, creates each x/2 value only when it is needed, uses ()
gen = (x/2 for x in range(5000000))
