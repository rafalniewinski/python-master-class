# https://github.com/austin-taylor/code-vault/blob/master/python_expert_notebook.ipynb
# https://docs.python.org/3/reference/datamodel.html

# pattern który zauważamy to: zawsze istnieje jakiś syntax jak + >, lub funkcja
# repr, len... oraz istnieje także protokół/dunder method który umożliwia
# zaimplementowanie tego zachowania

# syntax -> function
# x + y -> __add__
# init x -> __init__
# repr(x) -> __repr__
# x() -> __call__


class Polynomial:
    def __init__(self, *coeffs):
        self.coeffs = coeffs

    def __repr__(self):
        return 'Polynomial(*{!r})'.format(self.coeffs)

    def __add__(self, other):
        return Polynomial(*(x + y for x, y in zip(self.coeffs, other.coeffs)))

    def __len__(self):
        return len(self.coeffs)

    def __call__(self):
        pass


p1 = Polynomial(1, 2, 3)
p2 = Polynomial(3, 4, 3)


class Base:

    def foo(self):
        return self.bar()

    # wymuszenie implementacji na użytkowniku określonej metody 1 Sposób
    # def bar(self):
    #     raise NotImplementedError

    # wymuszenie implementacji na użytkowniku określonej metody 2 Sposób
    def __init_subclass__(cls, *a, **kw):
        assert hasattr(cls, 'bar')


class Derived(Base):

    @staticmethod
    def bar():
        print('it works!')


p = Derived()
p.foo()


class Animal:

    def __init__(self, length_of_list=10):
        if length_of_list != 0:
            self.values = [None] * length_of_list
        else:
            self.values = []

    def __getitem__(self, key):
        return self.values[key]

    def __setitem__(self, key, value):
        self.values[key] = value

    def __delitem__(self, key):
        del self.values[key]

    def __iter__(self):
        suprise = [1, 2, 3, 4, 5]
        return iter(suprise)

    def append(self, value):
        self.values.append(value)


test = Animal()

test[0] = "lorem"
test[1] = "ipsum"

for item in test:
    print(item)
