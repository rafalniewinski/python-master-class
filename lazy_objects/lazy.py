# http://zepworks.com/blog/python-magic-methods-and-lazy-objects/

# Why lazy?
# Better performace (depending on your design)
# Better illusion of performance (when dealing with heavy objects)
# Less hits to your database (depending on your design)

# q = Entry.objects.filter(headline__startswith="What")
# q = q.filter(pub_date__lte=datetime.date.today())
# q = q.exclude(body_text__icontains="food")
# print(q) # <-- evaluated here # wywołanie do bazy danych nastąpi w tym miejscu

NATIVE_ATTRIBUTES = {'class_name'}


class Root:
    def __init__(self):
        self.class_name = self.__class__.__name__.lower()

    def get_redis_key_path(self, key):
        return "{}.{}".format(self.class_name, key)

    def __getattr__(self, key):
        if key in NATIVE_ATTRIBUTES:
            return self.__dict__[key]
        else:
            key = self.get_redis_key_path(key)
            return Lazy(key)

    def __setattr__(self, key, value):
        if key in NATIVE_ATTRIBUTES:
            self.__dict__[key] = value
        else:
            key = self.get_redis_key_path(key)
            redis.set(key, value)


class Lazy:
    def __init__(self, key):
        self.key = key

    @property
    def value(self):
        # Redis keeps strings as bytes
        return redis.get(self.key).decode('utf-8')

    def __repr__(self):
        return "<Lazy {}>".format(self.key)

    def __str__(self):
        return self.value


root = Root()
root.something = 10

# does not evaluate the object yet since it runs __repr__
# Lazy    root.something >>> >

print(root.something)  # evaluates since it runs  __str__

# hmm w tym wpadku lazy_nest polega na tym że zapytanie do bazy danych idze
# tylko w momencie printowania lub użynia metody value na lazy-object
# wprowadzenie lazy-objectów nazwałbym nakładką
