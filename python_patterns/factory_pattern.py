from zipfile import ZipFile
import tarfile
import os

# first example


class BaseArchive(object):
    EXTENSION = None

    @classmethod
    def check_extenstion(cls, extension):
        return extension == cls.EXTENSION

    def __init__(self, location_path, files_to_pack):
        self.location_path = location_path
        self.files_to_pack = files_to_pack

    def generate(self):
        raise NotImplementedError()


class ZIPArchive(BaseArchive):
    EXTENSION = 'zip'

    def generate(self):
        with ZipFile('{}.{}'.format(self.location_path, self.EXTENSION), 'w') as zip_file:
            for file_ in self.files_to_pack:
                zip_file.write(file_)


class TARArchive(BaseArchive):
    EXTENSION = 'tar'

    def generate(self):
        with tarfile.open('{}.{}'.format(self.location_path, self.EXTENSION), 'w') as tar_file:
            for file_ in self.files_to_pack:
                tar_file.add(file_)


zip_archive = ZIPArchive(os.path.join(os.getcwd(), 'zip'), ['for_zip'])
zip_archive.generate()
tar_archive = TARArchive(os.path.join(os.getcwd(), 'tar'), ['for_tar.txt'])
tar_archive.generate()


class ArchiveManager(object):
    ARCHIVE_ENGINES = [ZIPArchive, TARArchive]

    def __init__(self, location_path, files_to_pack):
        self.location_path = location_path
        _, self.extension = os.path.splitext(location_path)
        self.files_to_pack = files_to_pack
        self.archive_engine = self.choose_archive_engine()

    def choose_archive_engine(self):
        for engine in self.ARCHIVE_ENGINES:
            if engine.check_extenstion(self.extension):
                return engine(self.location_path, self.files_to_pack)

    def create_archive(self):
        self.archive_engine.generate()

# another example