
import time
import logging
import pstats
import cProfile as profile

logger = logging.getLogger('my-awesome_logger')
logger.setLevel(logging.INFO)


def profile_wrapper(func):

    def wrapper(*args, **kwargs):
        p = profile.Profile()
        p.enable()
        start = time.clock()
        function = func(*args, **kwargs)
        end = time.clock() - start
        p.disable()
        # pstats.Stats(p).sort_stats('cumulative').print_stats(30)
        logger.info(func.__name__)
        logger.info(end)

        return function
    return wrapper


def extra_profile_wrapper(func):
    def wrapper(request, *args, **kwargs):
        p = profile.Profile()
        p.enable()
        start = time.clock()
        function = func(request, *args, **kwargs)
        end = time.clock() - start
        p.disable()
        # pstats.Stats(p).sort_stats('cumulative').print_stats(30)
        logger.info(func.__name__)
        logger