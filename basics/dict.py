# dictionaries
import argparse
import os
import functools
from collections import defaultdict, ChainMap
from copy import deepcopy

# dict methods:
# copy
# get
# items
# update
# pop
# popitem
# keys
# values
# fromkeys

# deklaracja:
default_dict = {
    "name": "norbert",
    "lastname": "niewiński",
    "pesel": 95090905751
}

secound_dict = dict(name="rafał", lastname="niewinski", pesel=95090905751)

name = default_dict.get("name", "Jarek")
print(name)

# copy
# obiekt został dodany także do copy_dict

awesome_dict = {1: [1, 2, 3]}
copy_dict = awesome_dict.copy()

awesome_dict[1].append(4)

print(awesome_dict, copy_dict)

awesome_dict = {1: [1, 2, 3]}
copy_dict = deepcopy(awesome_dict)

awesome_dict[1].append(4)

print(awesome_dict, copy_dict)

awesome_list = [1, 2, 3]
copy_list = awesome_list

copy_list = awesome_list

awesome_list.append(4)
print(awesome_list, copy_list)

# fromkeys

x = ['key1', 'key2', 'ke3']
random_list = [1, 2, 3]
this_dict = dict.fromkeys(x, random_list)

# keys / values / items

default_dict.keys()
default_dict.items()
default_dict.values()

# update

default_dict.update({"new key": 123})

# popitem / pop

# default_dict.popitem()
default_dict.pop("new key")

#
test = {True: 'yes', 1: 'no', 1.0: 'maybe'}
print(test)

# master dict skills
# four fundamental dict tool groping linking counting relations

master_dict = {
    "username": "Bragrim",
    "lorem": "raprs",
    "email": "niewinskirafal@o2.pl",
    "phone": "796 239 247"
}

# iter over keys
# dont do it !!
# for key in master_dict:
#     del master_dict[key]

# for loop in dict
d = {k: master_dict[k] for k in master_dict if not k.startswith('r')}

for k in d:
    print(k, '-->', d[k])

for k, v in master_dict.items():
    pass

# construct dict with two lists
names = ["roberto", "janek", "norbert", "rafał"]
colors = ["blue", "green", "yellow", "white"]

d = dict(zip(names, colors))

d = {}
for color in colors:
    d[color] = d.get(color, 0) + 1

d = defaultdict(int)

for color in colors:
    d[color] += 1

# grouping

names = ["roger", "betty", "melisa", "matthew", "charlie", "judith"]
d = {}
for name in names:
    key = len(name)
    if key not in d:
        d[key] = []
    d[key].append(name)

# poprawne rozwiązanie

d = {}
for name in names:
    key = len(name)
    d.setdefault(key, []).append(name)

# lub :)

d = defaultdict(list)
for name in names:
    key = len(name)
    d[key].append(name)

# linking / łączenie dictów.

defaults = {"color": "red", "user": "quest"}
parser = argparse.ArgumentParser()
parser.add_argument('-u', '--user')
parser.add_argument('-c', '--color')
namespace = parser.parse_args([])

command_line_args = {k: v for k, v in vars(namespace).items() if v}

# better solutions
d = defaults.copy()
d.update(os.environ)
d.update(command_line_args)


# the best colutions
d = ChainMap(command_line_args, os.environ, defaults)

a = {'a': 'A', 'c': 'C'}
b = {'b': 'B', 'c': 'D'}

m = ChainMap(a, b)

# problem stworzyc nowy dict który w values nie bedzie miał numerów

exercise_dict = {
    "key_1": "lorem4",
    "key_2": "loe3m",
    "key_3": "4lorem",
    "key_4": "lo1ew3"
}

import re

new_dict = {}
reg = re.compile('\d+')
for k, v in exercise_dict.items():
    new_value = reg.sub('', v)
    new_dict[k] = new_value

test = list[1:2, ..., 0]
