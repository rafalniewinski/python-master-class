import pandas as pd
from datetime import datetime


def update_data_task(company):
        next_pages, do_it = 1, True
        url = 'https://www.biznesradar.pl/notowania-historyczne/{0}'.format(company)
        while do_it:
            try:
                dfs = pd.read_html(url + ',{}'.format(next_pages))

                next_pages += 1
                table = dfs[0] # pd.read_html szuka tabelek na tej strony sa 3 tabelki, tam gdzie sa notowania to tabelka 0
                table.columns = table.iloc[0] # nadpisujemy kolumny table.iloc[o] wyciagamy pierwszy rekord w ktorym sa nazwy kolumn
                table = table[1:] # w table chcemy mieć same notowania.
                len_table = len(table)
                prices = [table.iloc[data_row].to_dict() for data_row in range(0, len_table)] # w ten sposob mamy table samych notowań

                for price in prices:
                        data = {
                            'symbol': company,
                            'timestamp': 'd',
                            'date': datetime.strptime(price['Data'],
                                                      '%d.%m.%Y'),
                            'open': float(price['Otwarcie']),
                            'max': float(price['Max']),
                            'min': float(price['Min']),
                            'close': float(price['Zamknięcie']),
                            'volume': float(price['Wolumen'].replace(' ',
                                                                     '')) if 'Wolumen' in price else 0,
                            'turnover': float(price['Obrót'].replace(' ', '')),
                        }
                        print(data)
            except Exception as e:
                print(str(e))
                print('Prawdopodobnie skonczyly sie nastepne strony')
                next_pages += 1
                do_it = False
        print('zakonczono pobieranie')


update_data_task('CCC')
