import xlwt
import pandas as pd

#######################################
# Programowanie obiektowe paradygmaty #
#######################################

# 1 Dziedziczenie:
# Możliwość współdzielenia atrybutów i funkcji jednej klasy przez drugą klase.

# 2 Abstrakcja:
# Przykład klasy abstrakcyjnej nie operujemy na konkretnych obiektach a na ich
# abstrakcjach, jak tworzymy klase Stół to nie mamy na myśli żadnego konkretnego
# stołu ale właśnie pewien abstrakcyjny obiekt bez dokładnych parametrów.

# 3 Polimorfizm:
# Przykład mamy 200 potworów różnych typów na mapie, niech każdy z nich
# użyje metody atakuj, każdy atak jest inny ale metoda ta sama. Przykład inny
# u nas export do xlsa działa na każdym indicatorze nie ważny od typu i zwórci
# inny csv dla różnych obiektow.

# 4 Hermetyzacja:
# Zamykanie pewnych atrybuów tak żeby mogłby przyjmować konkretne wartości
# np dla obiektu kwadrat jego bok zawsze musi być liczbą dodatnią za pomocą
# geterów i seterów jesteśmy w stanie narzucić wymiary tego boku.

# a private tylko w jednej klasie.
# b protected tylko pierwszy poziom dziedziczenia.
# c public dostępne wszedzie ?
# d default tego nie wiedzą nawet najstarsi magowie.

# interface -> klasa z samymi metodami bez ciała narzuca na inne klasy metody.
# klasa abrakcyjna -> to wszystko co zwykła klasa ale nie można stowrzyć.
# instacji po klasie abstrakcyjnej.

##############################
# Zasady programowania SOLID #
##############################

# S Single responsibility principle Zasada jednej odpowiedzialności.
# każda funkcja/klasa powinna być opowiedzialna za jedną rzecz
# powinien istnieć jeden powód do zmiany klasy

# Open/closed principle Zasada otwarte-zamknięte.
# klasy/Encje powinny być otwarte na rozszerzenia i zamknięte na modifikacje.

# Liskov substitution principle zasada podstawienia liskova.
# Każda metoda z interfejsu powinna byc implentowana w sposób przemyślany
# jeśli ryba dziedziczy po animal a animal ma metode run to niby co ona oznacza
# dla ryby ? ryba nie biega...

# Interface segregation principle zasada segregacji interfejsów.
# Wiele dedykowanych interfejsów jest lepsze niż jeden wielki ogólny.

# Dependency inversion principle zasada odwrócenia zależności.
# Zależności powinny wynikać z abstrakcji nie tworzymy funkcji które działają
# tylko dla jednego określonego przypadku/funkcji. Historia z programistą który
# przychodzi do pracy i mówi, że będzie pracował tylko na swoim biórku/laptopie.


class BaseIndicator:

    def __init__(self, quotations, name, dates=None):
            self.quotations = quotations
            self.dates = dates
            self._name = name
            self.indi_values = {}

    # 1 Paradygmat programowania obiektowego hermetyzacja

    @property
    def name(self, value):
        return self._name

    @name.setter
    def name(self, value):
        if type(value) is not str:
            raise ValueError("value must me string!")
        self._name = value

    def __repr__(self):
        return "Indicator({})".format(self.name)

    def __call__(self):
        print("call method run!")

    def __name__(self):
        return "{}".format(self.name)

    # wymuszenie implementacji na użytkowniku określonej metody.
    # def __new__(cls, name, bases, body):
    #     if not 'get_values_from_quotations' in body:
    #         raise TypeError("bad user class")
    #     return super().__new__(cls, name, bases, body)

    # wymuszenie implementacji na użytkowniku określonej metody.
    def get_values_from_quotations(self):
        raise NotImplementedError

    def get_indi_values_chart(self):
        pass

    # polimofrizm
    def export_to_xls(self):
        book = xlwt.Workbook()
        sheet1 = book.add_sheet("PySheet1", cell_overwrite_ok=True)

        standard_cols = ["Dates", "Quotations"]
        extra_cols = list(self.indi_values.keys())
        cols = standard_cols + extra_cols

        # zapisanie kolumn
        for num, col in enumerate(cols):
            row = sheet1.row(0)
            row.write(num, col)

        for num, quotation in enumerate(self.quotations, start=1):
            if len(self.quotations) == num:
                break
            row = sheet1.row(num)
            for index, col in enumerate(cols):
                if index == 0:
                    value = self.dates[num]
                    row.write(index, value)
                    continue
                if index == 1:
                    value = self.quotations[num]
                    row.write(index, value)
                    continue

                indi = self.indi_values.get(col)
                value = indi[num]

                row.write(index, value)

        book.save("indicator.xls")


# Solid 1 Simple Single Responsible
# każda klasa powinna mieć jedną odpiewiedzialność powinien istnieć jeden powód
# aby zmienić klase.

class Sma(BaseIndicator):

    def __init__(self, quotations, name, sma_length, dates=None):
        super(Sma, self).__init__(quotations, name, dates)
        self.sma_length = sma_length

    def get_values_from_quotations(self):
        values = []

        for num, quotation in enumerate(self.quotations):

            if num <= self.sma_length-1:
                values.append(0)
            else:
                last_quotations_num = num - self.sma_length - 1
                last_quotations = sum(self.quotations[last_quotations_num:num]) / self.sma_length
                values.append(last_quotations)

        self.indi_values.update({"sma": values})


class SmaDouble(BaseIndicator):

    def __init__(self, quotations, name, shorter_sma_length, longer_sma_length,
                 dates=None):
        super(SmaDouble, self).__init__(quotations, name, dates)
        self.shorter_sma_length = shorter_sma_length
        self.longer_sma_length = longer_sma_length

    def get_values_from_quotations(self):
        shorter_values = []
        longer_values = []

        for num, quotation in enumerate(self.quotations):

            if num <= self.shorter_sma_length - 1:
                shorter_values.append(0)
            else:
                last_quotations_num = num - self.shorter_sma_length - 1
                last_quotations = sum(self.quotations[
                                      last_quotations_num:num]) / self.shorter_sma_length
                shorter_values.append(last_quotations)

            if num <= self.longer_sma_length - 1:
                longer_values.append(0)
            else:
                last_quotations_num = num - self.longer_sma_length - 1
                last_quotations = sum(self.quotations[
                                      last_quotations_num:num]) / self.longer_sma_length
                longer_values.append(last_quotations)

        indi_values = {
            "shorter_sma": shorter_values,
            "longer_sma": longer_values
        }

        self.indi_values.update(indi_values)


class ExpertSma(Sma):

    start_capital = 100000

    def __init__(self, quotations, name, sma_length, dates=None):
        super(ExpertSma, self).__init__(quotations, name, sma_length, dates)
        self.sma_length = sma_length

    def run_expert(self):
        self.get_values_from_quotations()
        start_capital = self.start_capital
        start_capital_before_run = 100000
        sma = self.indi_values.get("sma")
        sma_length = self.sma_length
        trade_enable = True
        time_in_trade = 0
        buy = 0

        for num, quotation in enumerate(self.quotations, start=sma_length):
            # start zaczyna od sma_length ale dodaje także do końcówki + sma...
            if len(self.quotations) == num:
                break
            # buy signal
            if self.quotations[num] >= sma[num]:
                if trade_enable:
                    buy = start_capital / self.quotations[num]
                    trade_enable = False
                time_in_trade += 1
            if self.quotations[num] <= sma[num]:
                if time_in_trade != 0:
                    start_capital += buy * self.quotations[num] - buy \
                                     * self.quotations[num - time_in_trade]
                    trade_enable = True
                time_in_trade = 0

        profit = ((start_capital - start_capital_before_run)
                  / start_capital_before_run) * 100

        return profit


class ExpertSmaDouble(SmaDouble):
    start_capital = 100000

    def __init__(self, quotations, name, shorter_sma_length, longer_sma_length,
                 dates=None):
        super(ExpertSmaDouble, self).__init__(
            quotations, name, shorter_sma_length, longer_sma_length, dates)

    def run_expert(self):
        self.get_values_from_quotations()
        start_capital = self.start_capital
        start_capital_before_run = 100000
        shorter_sma = self.indi_values.get("shorter_sma")
        longer_sma = self.indi_values.get("longer_sma")
        longer_sma_length = self.longer_sma_length
        trade_enable = True
        time_in_trade = 0
        buy = 0

        for num, quotation in enumerate(self.quotations, start=longer_sma_length):
            if len(self.quotations) == num:
                break

            # buy signal
            if shorter_sma[num] >= longer_sma[num]:
                if trade_enable:
                    buy = start_capital / self.quotations[num]
                    trade_enable = False
                time_in_trade += 1
            if shorter_sma[num] <= longer_sma[num]:
                if time_in_trade != 0:
                    start_capital += buy * self.quotations[num] - buy \
                                     * self.quotations[num - time_in_trade]
                    trade_enable = True
                time_in_trade = 0

        profit = ((start_capital - start_capital_before_run)
                  / start_capital_before_run) * 100

        return profit


quotations_swig80 = pd.read_csv("swig80_d.csv", sep=';')

closed = list(quotations_swig80['Zamkniecie'])
dates = list(quotations_swig80['Data'])

expert_sma = ExpertSma(closed, name="sma", sma_length=33, dates=dates)
expert_sma_double = ExpertSmaDouble(
    closed, name="sma double", shorter_sma_length=16, longer_sma_length=33)

swig80_profit_sma = expert_sma.run_expert()
swig80_profit_sma_double = expert_sma_double.run_expert()

print(swig80_profit_sma)
print(swig80_profit_sma_double)

expert_sma.export_to_xls()
