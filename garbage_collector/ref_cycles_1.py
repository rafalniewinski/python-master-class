import gc
import ctypes

# przykład ref_cycles obiekt powiązany z samym sobą lub ob1=ob2 ob2=ob1.
# przez takie powiazanie nawet jak usuniemy obiekt wzanik ref bedzie na poziomie
# 1 przez co nie zostanie usuniety przez gc

# gc.collect force the Garbage Collector to release unreferenced memory
# By default, gc.garbage will be the list of all objects the gc couldn't clean up


class PyObject(ctypes.Structure):
    _fields_ = [("refcnt", ctypes.c_long)]

# aby coś się wyświetliło w gc trzeba to zaimplementować


gc.set_debug(gc.DEBUG_SAVEALL)

lst = []
lst.append(lst)

list_id = id(lst)

del lst
gc.collect()

for item in gc.garbage:
    if list_id == id(item):
        print("ref istnieje!")
