import weakref
# https://en.wikipedia.org/wiki/Weak_reference

# weakref czyli słabe referencje stworzenie wekref nie zwieksza licznika
# referencji, jeśli obiekt podstawowy zostanie usuniety to wekref zwróci None
# weak

# zastosowanie:
# weak references can break reference cycles
# observer pattern to avoid memory leak occurs oszczędzanie miejsca w pamięci ?


class Egg:

    def spam(self):
        print("I'm alive!")


obj = Egg()
weak_obj = weakref.ref(obj)

# odwołanie do ref jest poprzez wywołanie metody ()
weak_obj().spam()

obj = "Something else"

# Error !
weak_obj().spam()
