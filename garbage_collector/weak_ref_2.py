import weakref
from weakref import finalize

# Przykład 1 !


class Object:
    pass


o = Object()
r = weakref.ref(o)
o2 = o

print(o is o2)

# aby r() było None ref o2 też musi zostać zniszczona a wiec licznik musi
# spaść do 0

del o
print(r())

# Przykład 2
# możemy wprowadzić metode finalize która odpali się za każdym razem kiedy
# wykonamy coś na tym obiekcie ? albo call () lub del odapli się tylko raz !
# wraz z samą metodą potem będzie zwracała None.


class Object:
    pass


def lorem(lorem):
    print(lorem)


kenny = Object()
finalize(kenny, lorem, "You killed Kenny!")
del kenny

# Przykład 3
# finalize mozemy wywołać tylko raz !


def callback(x, y, z):
    print("CALLBACK")
    return x + y + z


obj = Object()
f = weakref.finalize(obj, callback, 1, 2, z=3)
assert f.alive
assert not f.alive

f()                     # callback not called because finalizer dead
del obj                 # callback not called because finalizer dead
