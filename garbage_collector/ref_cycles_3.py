import gc
import pprint
import gc
import ctypes
gc.set_debug(gc.DEBUG_SAVEALL)


class PyObject(ctypes.Structure):
    _fields_ = [("refcnt", ctypes.c_long)]


class Graph(object):
    def __init__(self, name):
        self.name = name
        self.next = None

    # metoda del odpala sie w momencie gc.collect() ?
    # def __del__(self):
    #     print('%s.__del__()' % self)

    def set_next(self, next_p):
        print('Linking nodes %s.next = %s' % (self, next_p))
        self.next = next_p

    def __repr__(self):
        return '%s(%s)' % (self.__class__.__name__, self.name)

# Construct a graph cycle


one = Graph('one')
two = Graph('two')
three = Graph('three')
one.set_next(two)
two.set_next(three)
three.set_next(one)

id_one = id(one)

print('three refers to:')
for r in gc.get_referents(three):
    pprint.pprint(r)

# Remove references to the graph nodes in this module's namespace
del one, two, three

# Show the effect of garbage collection

# print('Collecting %d ...')
# n = gc.collect()
# print('Unreachable objects:', n)
# print('Remaining Garbage:')
# pprint.pprint(gc.garbage)


print(PyObject.from_address(id_one).refcnt)
